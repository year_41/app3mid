package com.example.app3.data

import com.example.app3.R
import com.example.app3.model.Gas

class DataSource {
    fun loadGas(): List<Gas> {
        return listOf<Gas>(
            Gas(R.string.price1, R.string.title1),
            Gas(R.string.price2, R.string.title2),
            Gas(R.string.price3, R.string.title3),
            Gas(R.string.price4, R.string.title4),
            Gas(R.string.price5, R.string.title5),
            Gas(R.string.price6, R.string.title6),
            Gas(R.string.price7, R.string.title7),
            Gas(R.string.price8, R.string.title8),
            Gas(R.string.price9, R.string.title9)
        )
    }
}
